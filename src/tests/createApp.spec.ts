import { Application } from "express";
import request from "supertest"
import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from "mongoose";
import { v4 as uuidv4 } from 'uuid';

import createServer, {IServerConfig} from "../createServer";


let app:Application, mongod: MongoMemoryServer;

const auth = {
    user:'admin',
    password: 'admin'
}

beforeAll(async () => {
    // This will create an new instance of "MongoMemoryServer" and automatically start it
    mongod = await MongoMemoryServer.create();

    const uri = mongod.getUri();
    const config:IServerConfig = {
        databaseUrl: uri,
        users: {
            [auth.user]: auth.password,
        }
    }
    app = await createServer(config)
}, 20000)

afterAll(async ()=>{
    // close the ORM connection
    await mongoose.connection.close();
    // The Server can be stopped again with
    await mongod.stop();
} )

describe("The api/movies endpoint is rest-full", () => {
    test("POST creates a movie", async () => {
        const movieMock = {
            title: "Start Wars",
            year: 1998,
            genre: ["Action","Adventure"],
        }
        
        const response = await request(app)
            .post("/api/movies")
            .auth(auth.user,auth.password)
            .set("Content-Type", "application/json")
            .send(movieMock)
            .expect(200);

        expect(response.body).toMatchObject(movieMock)
    })
})

describe("GET /movies/:id", () => {    
    test("GET can fetch a movie correctly", async () => {
        // ARRANGE
        const movieMock = {
            title: "Start Wars",
            year: 1998,
            genre: ["Action","Adventure"]
        }

        // ACT
        const response = await request(app)
            .post("/api/movies")
            .auth(auth.user,auth.password)
            .set("Content-Type", "application/json")
            .send(movieMock);

        const getMovieResponse = await request(app)
        .get(`/api/movies/${response?.body?.id}`)
        .auth(auth.user,auth.password)
        .set("Accept", "application/json")     
        .expect(200)   
        .expect('Content-Type', /json/);

        // ASSERT
        expect(getMovieResponse.body).toMatchObject(movieMock) // Response Body
    })
    test("A not found resource returns 404", async () => {
        const randomId = uuidv4();
        await request(app)
        .get(`/api/movies/${randomId}`)
        .auth(auth.user,auth.password)
        .set("Accept", "application/json")     
        .expect(404)   
        .expect('Content-Type', /json/);
    })
    test("An invalid id returns 400", async () => {
        const invalidId = "invalid-id";
        await request(app)
        .get(`/api/movies/${invalidId}`)
        .auth(auth.user,auth.password)
        .set("Accept", "application/json")     
        .expect(400)   
        .expect('Content-Type', /json/);
    })
})


describe("DELETE /movies/:id", () => {    
    test("DELETE can delete a movie correctly", async () => {
        // ARRANGE
        const movieMock = {
            title: "Start Wars",
            year: 1998,
            genre: ["Action","Adventure"]
        }

        // ACT
        const response = await request(app)
            .post("/api/movies")
            .auth(auth.user,auth.password)
            .set("Content-Type", "application/json")
            .send(movieMock);

        const deleteMovieResponse = await request(app)
        .delete(`/api/movies/${response?.body?.id}`)
        .set("Accept", "application/json")   
        .auth(auth.user,auth.password)  
        .expect(200)   
        .expect('Content-Type', /json/);

        // ASSERT
        expect(deleteMovieResponse.body).toMatchObject(movieMock) // Response Body

        await request(app)
        .get(`/api/movies/${response?.body?.id}`)
        .set("Accept", "application/json")     
        .auth(auth.user,auth.password)
        .expect(404)   
        .expect('Content-Type', /json/);
    })
})



